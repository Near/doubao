all:report.tex
	pdflatex report
	make clean

clean:
	rm *.aux *.log
